Exploits = new Meteor.Collection("exploits");
Platforms = new Meteor.Collection("platforms");
Types = new Meteor.Collection("types");
Ports = new Meteor.Collection("ports");
Meteor.subscribe("exploits");
Meteor.subscribe("platforms");
Meteor.subscribe("types");
Meteor.subscribe("ports");

Router.configure({
  debug: false,
  before: function() {
    console.log('before all')
  }
});

Router.map(function() {
        this.route('exploitlist', { path: '/'});
        this.route('exploit', 
		{ path : '/exploit/:_id',
		  data: function() {
			console.log("** DATA **",Exploits.findOne(this));
			return Exploits.findOne(this.params._id);
		  },
		  template: 'exploitDetail'
		});
});

Template.thirtydays.getCount = function () {
	// sync call
        console.log("Fetching....");
        d = new Date();
        console.log("Date: ",d.format('yyyy-mm-dd HH:MM:ss'));
        d.setMonth( d.getMonth() - 1 );
        d.format('yyyy-MM-dd HH:MM:ss');
        console.log("Date: ",d.format('yyyy-mm-dd HH:MM:ss'));
        res = Exploits.find({
        date:
                { $gt: d.format("yyyy-mm-dd HH:MM:ss")
                }
        });
	console.log("results: ",res);
	var platforms = Platforms.find({},{ sort : { platform: 1 }});
	console.log("platforms: ",platforms);
	plist = null;
	var pcount = [];
	platforms.forEach(function (p) {
		//console.log("platform: ",p.platform);
		plist = Exploits.find({ platform : p.platform });
		console.log(p.platform," list: ", plist);
		if (plist.count() > 0) {
			i = [ p.platform , plist.count() ];
			pcount.push(i);
		}
	});	
	console.log("pcount: ",pcount);
        return res.count();
};

Template.oneday.getCount = function () {
        // sync call
	console.log("--------------------------------------------------------");
        console.log("Getting 72 Hour Count....");
        d = new Date();
        console.log(d,"Date: ",d.format('yyyy-mm-dd HH:MM:ss'));
        d.setDate( d.getDate() - 3 );
        d.setHours(00,00,00,00,00);
        d.format('yyyy-MM-dd HH:MM:ss');
        console.log("Date: ",d.format('yyyy-mm-dd HH:MM:ss'));
        res = Exploits.find({
        date:
                { $gt: d.format("yyyy-mm-dd HH:MM:ss")
                }
        });
        console.log("24 Hours: ",res);
        plist = null;
        var pcount = [];
        res.forEach(function (p) {
                //console.log("platform: ",p.platform);
                plist = Exploits.find( { date:
                { $gt: d.format("yyyy-mm-dd HH:MM:ss") }
                }, { platform : p.platform }
		);
                console.log(p.platform," list: ", plist);
                if (plist.count() > 0) {
                        i = [ p.platform , plist.count() ];
                        pcount.push(i);
                }
        });
        console.log("pcount: ",pcount);
	console.log("--------------------------------------------------------");
        return res.count();
};

Template.exploitlist.exploits = function() {
	return Exploits.find({},{ sort : { date: -1 }});
};

Template.exploitlist.decodeb64 = function(d) {
	var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

	code = Base64.decode(d);
	return hljs.highlightAuto(code).value;
	//return code;
};

Template.exploit.details = function(d) {

	console.log("***** Detail Template *****");
 var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

        code = Base64.decode(d);
        return hljs.highlightAuto(code).value;
};

Template.exploit.rendered = function() {
    console.log(this.data); // you should see your passage object in the console
};
